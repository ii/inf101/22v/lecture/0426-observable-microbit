package inf101v22.lecture0426.serialcom;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import javax.swing.JOptionPane;

import com.fazecast.jSerialComm.SerialPort;

/**
 * Handles communication with an input device communicating over a serial COM-connection.
 * For each received character, a call will be made to the `accept` method on the 
 * Conusmer<Character> object provided on construction.
 * 
 * A main method is provided for convenience, which simply outputs each received character
 * on a separate line on System.out.
 */
public class SerialComListener {

    public static void main(String[] args) {
        new SerialComListener(System.out::println).start();
    }

    private SerialPort port;
    private Thread thread;
    private Consumer<Character> onCharacterReceived;

    /**
     * Creates a new SerialComListener. This will bring up a GUI prompt asking which 
     * serial device to connect to. After connection is made (through a call to start),
     * the {@code accept} method on the provided {@link Consumer} will be called once
     * for each received character.
     * 
     * @param onCharacterReceived the accept method will be called once for each received character
     */
    public SerialComListener(Consumer<Character> onCharacterReceived) {
        this.onCharacterReceived = onCharacterReceived;
        this.port = (SerialPort) JOptionPane.showInputDialog(
                null,
                "Please select a serial port",
                "Serial port",
                JOptionPane.QUESTION_MESSAGE,
                null,
                SerialPort.getCommPorts(),
                null);
    }

    /** Start listening for input. */
    public void start() {
        this.thread = new Thread(this::run);
        this.thread.start();
    }

    private void run() {
        this.port.setBaudRate(115200);
        this.port.openPort();
        this.port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
        InputStream in = this.port.getInputStream();
        // BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (true) {
            try {
                // String s = br.readLine().strip();
                char c = (char) in.read();
                onCharacterReceived.accept(c);

            } catch (IOException e) {
                break;
            }
        }

        this.port.closePort();
    }

}
