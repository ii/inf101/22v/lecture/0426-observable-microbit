package inf101v22.lecture0426.app.model;

import inf101v22.lecture0426.observable.ControlledObservable;
import inf101v22.lecture0426.observable.Observable;

public class TwoSensorModel implements TwoSensorViewable {

    private final ControlledObservable<Boolean> buttonA = new ControlledObservable<Boolean>(false);
    private final ControlledObservable<Boolean> buttonB = new ControlledObservable<Boolean>(false);

    @Override
    public Observable<Boolean> getButtonA() {
        return this.buttonA;
    }

    @Override
    public Observable<Boolean> getButtonB() {
        return this.buttonB;
    }

    /** Set the state of button A. */
    public void receiveDataA(boolean newValue) {
        this.buttonA.setValue(newValue);
    }

    /** Set the state of button B. */
    public void receiveDataB(boolean newValue) {
        this.buttonB.setValue(newValue);
    }
    
}
