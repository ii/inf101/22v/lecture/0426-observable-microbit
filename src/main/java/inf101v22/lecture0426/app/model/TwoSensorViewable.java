package inf101v22.lecture0426.app.model;

import inf101v22.lecture0426.observable.Observable;

public interface TwoSensorViewable {
    
    /** Get an observable for the current state of button A. */
    Observable<Boolean> getButtonA();

    /** Get an observable for the current state of button B. */
    Observable<Boolean> getButtonB();

}
