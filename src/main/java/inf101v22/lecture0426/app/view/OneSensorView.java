package inf101v22.lecture0426.app.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import inf101v22.lecture0426.observable.Observable;

public class OneSensorView extends JComponent {

    private final Observable<Boolean> value;

    public OneSensorView(Observable<Boolean> value) {
        this.value = value;
        this.value.addObserver(this::repaint);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Color color = this.value.getValue() ? Color.RED : Color.BLACK;
        // The line above is the same as:
        // Color color;
        // if (this.value.getValue()) {
        //     color = Color.RED;
        // } else {
        //     color = Color.BLACK;
        // }

        g.setColor(color);
        g.fillOval(0, 0, this.getWidth(), this.getHeight());
    }

    @Override
    public Dimension preferredSize() {
        return new Dimension(200, 200);
    }
}
