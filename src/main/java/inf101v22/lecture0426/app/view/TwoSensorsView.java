package inf101v22.lecture0426.app.view;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import inf101v22.lecture0426.app.model.TwoSensorViewable;

public class TwoSensorsView extends JComponent {

    public TwoSensorsView(TwoSensorViewable model) {
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(new OneSensorView(model.getButtonA()));
        this.add(new OneSensorView(model.getButtonB()));
    }
    
}
