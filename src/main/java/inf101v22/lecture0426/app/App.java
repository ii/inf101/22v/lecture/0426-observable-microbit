package inf101v22.lecture0426.app;

import javax.swing.JComponent;
import javax.swing.JFrame;

import inf101v22.lecture0426.app.controller.TwoSensorController;
import inf101v22.lecture0426.app.model.TwoSensorModel;
import inf101v22.lecture0426.app.view.TwoSensorsView;

public class App {

    public static void main(String[] args) {
        new App();
    }
    
    public App() {
        JFrame frame = new JFrame("INF101 Microbit two sensors");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        TwoSensorModel model = new TwoSensorModel();
        new TwoSensorController(model);
        JComponent view = new TwoSensorsView(model);
        frame.setContentPane(view);

        frame.pack();
        frame.setVisible(true);
    }

}
