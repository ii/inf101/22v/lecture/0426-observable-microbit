package inf101v22.lecture0426.app.controller;

import inf101v22.lecture0426.app.model.TwoSensorModel;
import inf101v22.lecture0426.serialcom.SerialComListener;

public class TwoSensorController {
    
    private TwoSensorModel model;

    public TwoSensorController(TwoSensorModel model) {
        this.model = model;

        SerialComListener com = new SerialComListener(this::doThisWhenNewCharacterArrives);
        com.start();
    }

    private void doThisWhenNewCharacterArrives(char c) {
        if (c == 'a') {
            this.model.receiveDataA(false);
        }
        else if (c == 'A') {
            this.model.receiveDataA(true);
        }
        else if (c == 'b') {
            this.model.receiveDataB(false);
        }
        else if (c == 'B') {
            this.model.receiveDataB(true);
        }
    }
}
