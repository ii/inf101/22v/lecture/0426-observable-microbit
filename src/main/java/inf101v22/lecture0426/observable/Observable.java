package inf101v22.lecture0426.observable;

public interface Observable <E> {
    
    /** Adds a (non-null) observer. */
    public void addObserver(Observer observer);

    /**
     * Removes a (non-null) observer. Returns true if the observer was removed, and
     * false if the observer was not found.
     */
    public boolean removeObserver(Observer observer);
    

    /** Gets the current value. */
    public E getValue();
}
