package inf101v22.lecture0426.observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ControlledObservable<E> implements Observable<E> {

    private List<Observer> observers = new ArrayList<>();
    private E value;

    /** Create a new observable with the given initial value. */
    public ControlledObservable(E initialValue) {
        this.value = initialValue;
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public boolean removeObserver(Observer observer) {
        return this.observers.remove(observer);
    }

    @Override
    public E getValue() {
        return this.value;
    }

    /**
     * Sets the value of this observable variable. If the new value is 
     * different from the old value, all observers will be notified that
     * the value has changed.
     * 
     * @param newValue
     * @return true if the the new value was different from the old value, false otherwise
     */
    public boolean setValue(E newValue) {
        if (Objects.equals(this.value, newValue)) {
            return false;
        }

        this.value = newValue;
        this.notifyObservers();
        return true;
    }

    private void notifyObservers() {
        for (Observer obs : this.observers) {
            obs.update();
        }
    }
    
}
