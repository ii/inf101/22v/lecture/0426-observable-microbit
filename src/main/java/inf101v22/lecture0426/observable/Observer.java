package inf101v22.lecture0426.observable;

public interface Observer {

    /** Called when the observed value is updated. */
    void update();
    
}
