package inf101v22.lecture0426.observable;

public class SeparateFileObserver implements Observer {

    private TestObservable parent;

    public SeparateFileObserver(TestObservable parent) {
        this.parent = parent;
    }

    @Override
    public void update() {
        this.parent.setFlag0ToTrue();
    }

}
